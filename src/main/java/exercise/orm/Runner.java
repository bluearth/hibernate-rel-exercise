package exercise.orm;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class Runner {

	public static void main(String[] args) {

		Configuration config = new Configuration();
		// This will load the default hibernate.cfg.xml at the root of classpath
		config.configure(); 
		
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(config.getProperties())
				.build();
		
		SessionFactory sessionFactory = config.buildSessionFactory(serviceRegistry);
		
		// Use sessionFactory to open session each time needed
		Session session = sessionFactory.openSession();
		
		// Use session to persist or load persistent objects
		// ...

		
		session.flush();
		session.close();
		sessionFactory.close();
	}
}
